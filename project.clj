(defproject clj-orbit "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "EPL-2.0 OR GPL-2.0-or-later WITH Classpath-exception-2.0"
            :url "https://www.eclipse.org/legal/epl-2.0/"}
  :dependencies [[org.clojure/clojure "1.10.0"]
                 [cloud.orbit/orbit-core "1.14.0"]
                 [cloud.orbit/orbit-runtime "1.14.0"]
                 [cloud.orbit/orbit-json "1.14.0"]
                 [cloud.orbit/orbit-redis-cluster "1.5.0"]]
  :source-paths ["src/clj"]
  :java-source-paths ["src/java"]
  :profiles {:uberjar {:aot :all
                       :main temp.main }}
  :repl-options {:init-ns temp.core})
