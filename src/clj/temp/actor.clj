(ns temp.actor
  (:import (cloud.orbit.concurrent Task)
           (cloud.orbit.actors ObserverManager)
           (java.util.function Consumer)
           (temp IObserver)))

(gen-class
  :name temp.Actor
  :post-init initState
  :exposes {state {:get getState :set setState}}
  :implements [temp.Hello]
  :extends cloud.orbit.actors.runtime.AbstractActor)

(defn- observer-manager [this]
  (:observers @(.getState this)))

(defn -initState [this]
  (.setState this (atom {:observers (ObserverManager.)})))

(defn -addObserver [this observer]
  (let [manager (observer-manager this)]
    (.addObserver manager observer)
    (Task/done)))

(defn -sayHello [this greeting]
  (.notifyObservers (observer-manager this)
                    (reify
                      Consumer
                      (accept [_ o]
                        (.someEvent ^IObserver o "Observation!"))))
  (Task/fromValue (str "You said: '" greeting "', I say: Hello from " + (System/identityHashCode this) " !")))

(defn -doWriteState [this]
  (.writeState this))
