(ns temp.core
  (:require [temp.actor])
  (:import (cloud.orbit.actors Stage$Builder Actor)
           (temp Hello IObserver Observer)
           (cloud.orbit.actors.cluster RedisClusterBuilder)
           (cloud.orbit.concurrent Task)
           (cloud.orbit.actors.extensions.json InMemoryJSONStorageExtension)))

(defn create-stage []
  (let [cluster-peer (-> (RedisClusterBuilder.) (.messagingUri "redis://localhost/") (.build))
        stage (-> (Stage$Builder.)
                  (.clusterName "orbit-helloworld-cluster")
                  (.clusterPeer cluster-peer)
                  (.extensions [(InMemoryJSONStorageExtension.)])
                  (.build))]
    (-> stage (.start) (.join))
    (.bind stage)
    stage))

(defn run []
  (let [stage (create-stage)
        ^Hello hello-actor (Actor/getReference Hello "0")
        _ (.join (.addObserver hello-actor
                               (Observer.
                                (reify
                                  IObserver
                                  (someEvent [_ message]
                                    (println "observer got message:'" message "'")
                                    (Task/done))))))
        _ (.join (.doWriteState hello-actor))
        response (.join (.sayHello hello-actor "Welcome to orbit!"))]
    (println response)
    (-> stage (.stop) (.join))))
