(ns temp.main
  (:gen-class)
  (:require temp.core
            temp.actor)
  (:import (temp Hello)
           (cloud.orbit.actors Actor)))

(defn -main [& args]
  (temp.core/run))
