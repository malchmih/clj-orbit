package temp;

import cloud.orbit.actors.Actor;
import cloud.orbit.concurrent.Task;

public interface Hello extends Actor {
    Task<String> sayHello(String greeting);
    Task addObserver(IObserver observer);
    Task doWriteState();
}
