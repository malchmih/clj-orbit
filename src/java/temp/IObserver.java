package temp;

import cloud.orbit.actors.ActorObserver;
import cloud.orbit.actors.annotation.OneWay;
import cloud.orbit.concurrent.Task;

public interface IObserver extends ActorObserver {
    @OneWay
    Task someEvent(String message);
}
