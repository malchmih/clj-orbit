package temp;

import cloud.orbit.concurrent.Task;

public class Observer implements IObserver {
    private IObserver impl;

    public Observer(IObserver impl) {
        this.impl = impl;
    }

    @Override
    public Task someEvent(String message) {
        return impl.someEvent(message);
    }
}
